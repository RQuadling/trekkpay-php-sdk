<?php

namespace TrekkPay\Sdk\ApiClient\Methods;

use TrekkPay\Sdk\ApiClient\Http\Response;

final class Transactions extends MethodsCollection
{
    /**
     * @param int|int[] $merchantIds
     * @param array     $params
     *
     * @return Response
     */
    public function search($merchantIds, array $params = [])
    {
        $params['merchant_ids'] = (array) $merchantIds;

        return $this->request('transactions.search', $params);
    }

    /**
     * @param int|int[] $merchantIds
     * @param array     $params
     *
     * @return Response
     */
    public function getStats($merchantIds, array $params = [])
    {
        $params['merchant_ids'] = (array) $merchantIds;

        return $this->request('transactions.getStats', $params);
    }
}
